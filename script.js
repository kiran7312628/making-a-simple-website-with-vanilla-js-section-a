let submitbtn = document.getElementById("submitbtn");
let modalclosebtn = document.getElementsByClassName("btn-close")[0];

//On submit of form
const formsubmit =async (event)=>{
  
  //CSS loader
document.getElementsByClassName("loader")[0].style.display="block";

  let name = document.getElementById("name").value;
  let email = document.getElementById("email").value;
  let message = document.getElementById("ordercomments").value;

  if(name.trim()==="" || message.trim()==="" || validateEmail(email)===false)  //stop execution if the fields data is not correct or empty
  {
    document.getElementsByClassName("loader")[0].style.display="none";
    return ;
  }
  event.preventDefault();
  console.log("Hi");

  var myHeaders = new Headers();
  myHeaders.append("Code", "I1NFZTVKFKI97RBJQQLOMKOF0");
  
  var formdata = new FormData();
  formdata.append("firstname", name);
  formdata.append("email", email);
  formdata.append("message", message);

  
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: formdata,
  };
  
  fetch('https://cors-anywhere.herokuapp.com/https://forms.maakeetoo.com/formapi/721', requestOptions)
    .then(response =>{ return response.text()})
    .then(result => 
      {      modalclosebtn.click();
             console.log(result);
             document.getElementsByClassName("loader")[0].style.display="none";
            alert("Order request successfull");
      })
    .catch(error => {console.log('error', error); document.getElementsByClassName("loader")[0].style.display="none";});
}

submitbtn.addEventListener('click',formsubmit)  //Event listener of pricing buttons


//Email validation using Regex patterns because we are using event.preventDefault()
function validateEmail(email) {
  const res = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
  return res.test(String(email).toLowerCase());
}


//Slider to select number of users
let slider = document.getElementById("slider");
slider.addEventListener("input",userfunc);

function userfunc() {
  let selectedusers = document.getElementById("selectedusers");

  selectedusers.innerText=slider.value;


  console.log(slider.value)
  let cardslist = document.getElementsByClassName("card");
  Array.from(cardslist).forEach(element => {
     element.style.border="none";
  });
  if(slider.value==0)
  {
    return;
  }

 document.getElementById(`card${slider.value}`).style.border="5px solid darkturquoise";
}